class AnimeNotFoundError(Exception):
    def __init__(self, message, code=404) -> None:
        self.message = message
        self.code = code


class AnimeKeysInvalidError(Exception):
    
    tipos = ["anime", "released_date", "seasons"]

    def __init__(self, columns, code=422):
        self.message = {
            "tipos esperados": f"{AnimeKeysInvalidError.tipos}",
            "tipos recebidos": f"{columns}",
        }
        self.code = code
        super().__init__(self.message)