from dotenv import load_dotenv
from os import getenv

import psycopg2

load_dotenv()

configs = {
  "host": getenv("HOST"),
  "database": getenv("DB"),
  "user": getenv("USER"),
  "password": getenv("PASS")
}


def conn_cur():
    conn = psycopg2.connect(**configs)
    cur = conn.cursor()

    return conn, cur


def commit_and_close(conn, cur):
    conn.commit()
    cur.close()
    conn.close()


conn, cur = conn_cur()
cur.execute("""
    CREATE TABLE IF NOT EXISTS animes (
        anime_id BIGSERIAL PRIMARY KEY,
        anime VARCHAR(100) UNIQUE NOT NULL,
        released_date DATE NOT NULL,
        seasons INTEGER NOT NULL
);
""")
commit_and_close(conn, cur)