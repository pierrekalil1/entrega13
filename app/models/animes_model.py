from . import conn_cur, commit_and_close
from app.exc.exceptions import AnimeNotFoundError
from psycopg2 import sql


class AnimeModel():

    series_keys = ["anime_id", "anime", "released_date", "seasons"]
    keys = [ "anime", "released_date", "seasons"]

    def __init__(self, fields)  -> None:
        
        if type(fields) is tuple:
            self.amine_id, self.anime, self.released_date , self.seasons = fields

        elif type(fields) is dict:
            self.anime = fields["anime"].title()
            self.released_date = fields["released_date"] 
            self.seasons = fields["seasons"]  

    
    @staticmethod
    def get_all():
        try:
            conn, cur = conn_cur()
            cur.execute(
              """
              SELECT * FROM animes;
              """
            )
            animes = cur.fetchall()

            commit_and_close(conn, cur)

            animes_found = [dict(zip(AnimeModel.series_keys, anime)) for anime in animes]

            return {"data": animes_found}

        except :
            return {"data": []}
    

    @staticmethod
    def get_by_id(id):
        conn, cur = conn_cur()
        cur.execute(
        """
        SELECT * FROM animes WHERE anime_id=(%s);
        """, (id,),
        )
        anime = cur.fetchone()

        if not anime:
            raise AnimeNotFoundError(f"Anime de id {id} não encontrado")

        commit_and_close(conn, cur)
        anime_info = AnimeModel(anime).__dict__

        return anime_info


    def create_anime(self):
        conn, cur = conn_cur()

        query = """
            INSERT INTO 
                animes 
            (anime, released_date, seasons ) 
            VALUES 
                (%s, %s, %s)
            RETURNING *
        """

        query_values = list(self.__dict__.values())
        cur.execute(query, query_values)
        inserted_data = cur.fetchone()

        # if inserted_data:
        #     raise AnimeNotFoundError(f"Anime de id {self.anime} já esta cadastrado")
   
        commit_and_close(conn, cur)
        return AnimeModel(inserted_data).__dict__


    def delete_anime(id):
        conn, cur = conn_cur()
        query = """
            DELETE FROM
                animes
            WHERE
                anime_id=(%s)
            RETURNING *
        """
        cur.execute(query, (id,))
        
        anime = cur.fetchone()

        if not anime:
            raise AnimeNotFoundError(f"Anime de id {id} não encontrado")

        commit_and_close(conn, cur)
        anime_info = AnimeModel(anime).__dict__

        return anime_info

    
    def update_anime(id, payload: dict):

        conn, cur = conn_cur()

        columns = [sql.Identifier(key) for key in payload.keys()]
        values = [sql.Literal(value) for value in payload.values()]

        query = sql.SQL(
            """
                UPDATE
                    animes
                SET
                    ({columns}) = row({values})
                WHERE
                    anime_id={id}
                RETURNING *
            """
        ).format(
            id=sql.Literal(id),
            columns=sql.SQL(",").join(columns),
            values=sql.SQL(",").join(values),
        )

        cur.execute(query)
        update_anime = cur.fetchone()

        if not update_anime:
            raise AnimeNotFoundError(f"Anime de id {id} não encontrado")

        commit_and_close(conn, cur)

        return dict(zip(AnimeModel.series_keys, update_anime))