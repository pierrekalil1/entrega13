from app.exc.exceptions import AnimeKeysInvalidError, AnimeNotFoundError
from app.models.animes_model import AnimeModel
from flask import jsonify, request
from psycopg2.errors import UniqueViolation


def get_create():
    
    if request.method == 'POST':

        try:
            data = request.get_json()
            list_keys = list(data.keys())

            for key in list_keys:
                if key not in AnimeModel.keys:
                    raise AnimeKeysInvalidError(f"A(s) chave(s) {list_keys} não existe(m)")

            anime = AnimeModel(data)
            return jsonify(anime.create_anime())

        except AnimeKeysInvalidError as e:
            return jsonify({"error": e.message}), e.code
        except UniqueViolation:
            return jsonify({"error": f"Anime {data['anime']} já está cadastrado"}), 409

    else:
        return jsonify(AnimeModel.get_all()), 200

        
def filter(id):
    try:
        anime = AnimeModel.get_by_id(id)
    except AnimeNotFoundError as e:
        return jsonify({"msg": e.message}), e.code

    return jsonify(anime), 200


def delete(id):
    try:
        anime = AnimeModel.delete_anime(id)
    except AnimeNotFoundError as e:
        return jsonify({"msg": e.message}), e.code
    return jsonify(anime), 200


def update(id):
    
    try:
        data = request.get_json()
        list_keys = list(data.keys())

        for key in list_keys:
            if key not in AnimeModel.keys:
                raise AnimeKeysInvalidError(f"A(s) chave(s) {list_keys} não existe(m)")

        anime = AnimeModel.update_anime(id, data)
        return jsonify(anime), 200
    
    except AnimeNotFoundError as e:
        return jsonify({"msg": e.message}), e.code
    except AnimeKeysInvalidError as err:
        return jsonify({"msg": err.message}), err.code