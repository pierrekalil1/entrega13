from flask import Blueprint
from app.controllers.anime_controller import delete, get_create, filter, update

bp = Blueprint("bp_animes", __name__, url_prefix="/animes" )


bp.route("", methods=('GET', 'POST'))(get_create)
bp.get("/<int:id>")(filter)
bp.delete("/<int:id>")(delete)
bp.patch("/<int:id>")(update)
